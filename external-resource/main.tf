
resource "aws_api_gateway_method" "method" {
  count         = var.method != "" ? 1 : 0
  rest_api_id   = var.apigateway_id
  resource_id   = var.external_resource
  http_method   = var.method
  authorization = var.authorization
  authorizer_id = var.authorizer_id

  request_models = {
    "application/json" = "Error"
  }
  request_parameters = var.request_parameters_method

}
resource "aws_api_gateway_integration" "integration" {
  count         = var.method != "" ? 1 : 0
  rest_api_id = var.apigateway_id
  resource_id = var.external_resource
  http_method = aws_api_gateway_method.method[0].http_method
  uri                     =  var.uri
  integration_http_method = var.method
  connection_type = "VPC_LINK"
  type = "HTTP_PROXY"
  connection_id   = var.vpc_link
  request_parameters = var.request_parameters_integration
}
resource "aws_api_gateway_method_response" "response" {
  count         = var.method != "" ? 1 : 0
  rest_api_id = var.apigateway_id
  resource_id = var.external_resource
  http_method = aws_api_gateway_method.method[0].http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
        "method.response.header.Access-Control-Allow-Origin" = true,
    }
}

resource "aws_api_gateway_integration_response" "response" {
   count         = var.method != "" ? 1 : 0
   rest_api_id = "${var.apigateway_id}"
   resource_id = "${var.external_resource}"
   http_method = "${aws_api_gateway_method.method[0].http_method}"
   status_code = "${aws_api_gateway_method_response.response[0].status_code}"

   response_templates = {
       "application/json" = ""
   }
   response_parameters = {
        "method.response.header.Access-Control-Allow-Origin" = var.allowOrigin,
    } 
}

# resource "aws_api_gateway_method" "options_method" {
#     count         = var.cors != null ? 1 : 0
#     rest_api_id   = var.apigateway_id
#     resource_id   = "${var.external_resource}"
#     http_method   = "OPTIONS"
#     authorization = "NONE"
# }
# resource "aws_api_gateway_method_response" "options_200" {
#     count         = var.cors != null ? 1 : 0
#     rest_api_id   = var.apigateway_id
#     resource_id   = "${var.external_resource}"
#     http_method   = "${aws_api_gateway_method.options_method[0].http_method}"
#     status_code   = "200"
#     response_models = {
#         "application/json" = "Empty"
#     }
#     response_parameters = {
#         "method.response.header.Access-Control-Allow-Headers" = true,
#         "method.response.header.Access-Control-Allow-Methods" = true,
#         "method.response.header.Access-Control-Allow-Origin" = true
#     }
#     depends_on = ["aws_api_gateway_method.options_method"]
# }
# resource "aws_api_gateway_integration" "options_integration" {
#     count         = var.cors != null ? 1 : 0
#     rest_api_id   = var.apigateway_id
#     resource_id   = "${var.external_resource}"
#     http_method   = "${aws_api_gateway_method.options_method[0].http_method}"
#     type          = "MOCK"
#     depends_on = ["aws_api_gateway_method.options_method"]
#     request_templates    = {
#        "application/json" = jsonencode(
#               {
#                  statusCode = 200
#               }
#           )
#       }
# }
# resource "aws_api_gateway_integration_response" "options_integration_response" {
#     count         = var.cors != null ? 1 : 0
#     rest_api_id   = var.apigateway_id
#     resource_id   = "${var.external_resource}"
#     http_method   = "${aws_api_gateway_method.options_method[0].http_method}"
#     status_code   = "${aws_api_gateway_method_response.options_200[0].status_code}"
#     response_parameters = {
#         "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,${var.cors}'",
#         "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS,PATCH,PUT,POST'",
#         "method.response.header.Access-Control-Allow-Origin" = var.allowOrigin
#     }
#      response_templates = {
#        "application/json" = ""
#    }
#     depends_on = ["aws_api_gateway_method_response.options_200"]
# }
