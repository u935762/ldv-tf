variable "path" {
  type        = string
  default = ""
}
variable "external_resource" {
  type        = any
  default = null
}

variable "apigateway_id" {
  type        = string
  default = ""
}
variable "parent_id" {
  type        = string
  default = ""
}
variable "method" {
  type        = string
  default = ""
}
variable "authorization" {
  type        = string
  default = "NONE"
}
variable "authorizer_id" {
  type        = string
  default = ""
}
variable "uri" {
  type        = string
  default = ""
}
variable "vpc_link"{
  type  = string
  default = ""
}
variable "request_parameters_method" {
  type  = map(any)
  default = null
}
variable "request_parameters_integration" {
  type  = map(any)
  default = null
}
variable "cors" {
  type  = string
  default = null
}
variable allowOrigin {
  type        = string
  default     = "'*'"
}
