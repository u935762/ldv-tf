## Ejemplo de recurso completo
```
module "X"{
  source = "git::https://gitlab.com/telecom-argentina/devops/iac/terraform/modules/aws/aws-apigateway-resource.git//default?ref=2.0.0"
  path = "{lineNumber}"
  apigateway_id = data.terraform_remote_state.iac_apigateway_state.outputs.apigateway_id
  parent_id = data.terraform_remote_state.iac_apigateway_state.outputs.apigateway_root_resource_id


  method = "GET"
  authorization = "CUSTOM"
  authorizer_id = data.terraform_remote_state.iac_apigateway_state.outputs.authorizer_id
  uri = "http://${data.terraform_remote_state.iac_apigateway_state.outputs.nlb_dns_name}/xxx/{lineNumber}"
  vpc_link = data.terraform_remote_state.iac_apigateway_state.outputs.vpc_link_id
  #cors = "x-global-transaction-id,x-source-version,x-source-name"
  request_parameters_method = {
    "method.request.path.lineNumber" = true
    "method.request.header.Authorization" = true
  }
  request_parameters_integration = {
    "integration.request.path.lineNumber" = "method.request.path.lineNumber"
    "integration.request.header.Authorization" = "method.request.header.Authorization"
  }
}
```
## Ejemplo de metodo referenciando a otro recurso
```

module "XY"{
  source = "git::https://gitlab.com/telecom-argentina/devops/iac/terraform/modules/aws/aws-apigateway-resource.git//external-resource?ref=2.0.0"
  apigateway_id = data.terraform_remote_state.iac_apigateway_state.outputs.apigateway_id
  parent_id = data.terraform_remote_state.iac_apigateway_state.outputs.apigateway_root_resource_id
  external_resource = module.X.resource.id

  method = "POST"
  authorization = "CUSTOM"
  authorizer_id = data.terraform_remote_state.iac_apigateway_state.outputs.authorizer_id
  uri = "http://${data.terraform_remote_state.iac_apigateway_state.outputs.nlb_dns_name}/xxx/{lineNumber}"
  vpc_link = data.terraform_remote_state.iac_apigateway_state.outputs.vpc_link_id
  cors = "x-global-transaction-id,x-source-version,x-source-name"
  request_parameters_method = {
    "method.request.path.lineNumber" = true
    "method.request.header.Authorization" = true
  }
  request_parameters_integration = {
    "integration.request.path.lineNumber" = "method.request.path.lineNumber"
    "integration.request.header.Authorization" = "method.request.header.Authorization"
  }
}
```
